<?php

require_once("config.php");

$usuari = $_POST['nombre'];
$credencial = $_POST['contrassenya'];

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $query = $conn->prepare("SELECT * FROM usuaris_app WHERE nom =:usuari");
    $query->bindParam("usuari", $usuari, PDO::PARAM_STR);
    $query->execute();
    $existingUser = $query->fetch(PDO::FETCH_ASSOC);

    if ($existingUser) {
        echo json_encode(array('estat' => 'KO', 'error' => 'L’usuari existeix', 'usuari_app' => $usuari));
    } else if($usuari!="") {
        $query = $conn->prepare("INSERT INTO usuaris_app (nom, password) VALUES (:usuari, :credencial)");
        $query->bindParam(":usuari", $usuari, PDO::PARAM_STR);
        $query->bindParam(":credencial", $credencial, PDO::PARAM_STR);
        $query->execute();
        echo json_encode(array('estat' => 'OK', 'error' => '', 'usuari_app' => $usuari));
    } else {
        echo json_encode(array('estat' => 'KO', 'error' => 'usuari incorrecte', 'usuari_app' => $usuari));
    }
} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
