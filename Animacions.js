//animacio header

const header = document.getElementById("header");
header.style.backgroundColor="lime";

moving();
function moving(){
    const headeranimation=[
        {backgroundColor: "indianred"},
        {backgroundColor: "cornflowerblue"},
        {backgroundColor: "darkgrey"}
    ];
    const headertiming={
        duration:5000,
        iterations: Infinity,
        direction: "alternate"
    };
    header.animate(headeranimation,headertiming);
}

//animacio rotacio imatge

$(document).ready(function() {
    var animatedElement = $("#imatgeasuna");

    animatedElement.animate({
        width: "400px",
        height: "600px"
    }, {
        duration: 1000,
        step: function(now, fx) {
            if (fx.prop === "width" || fx.prop === "height") {
                animatedElement.css("transform", "rotate(" + now * 360 / 600 + "deg)");
            }
        },
        complete: function() {
        }
    });
});

//animacio nav

$("#li1").on("mouseenter", function (event) {
    $('#li1').css('font-weight', 'bold');
});

$("#li1").on("mouseleave", function (event) {
    $('#li1').css('font-weight', 'normal');
});
$("#li2").on("mouseenter", function (event) {
    $('#li2').css('font-weight', 'bold');
});

$("#li2").on("mouseleave", function (event) {
    $('#li2').css('font-weight', 'normal');
});

$("#li3").on("mouseenter", function (event) {
    $('#li3').css('font-weight', 'bold');
});

$("#li3").on("mouseleave", function (event) {
    $('#li3').css('font-weight', 'normal');
});

$("#li4").on("mouseenter", function (event) {
    $('#li4').css('font-weight', 'bold');
});

$("#li4").on("mouseleave", function (event) {
    $('#li4').css('font-weight', 'normal');
});
$("#li5").on("mouseenter", function (event) {
    $('#li5').css('font-weight', 'bold');
});

$("#li5").on("mouseleave", function (event) {
    $('#li5').css('font-weight', 'normal');
});
$("#li6").on("mouseenter", function (event) {
    $('#li6').css('font-weight', 'bold');
});

$("#li6").on("mouseleave", function (event) {
    $('#li6 ').css('font-weight', 'normal');
});
$("#li7").on("mouseenter", function (event) {
    $('#li7').css('font-weight', 'bold');
});

$("#li7").on("mouseleave", function (event) {
    $('#li7  ').css('font-weight', 'normal');
});