<?php

require_once("config.php");

$nouUsuari = $_POST['nombre'];
$novaContra = $_POST['contrassenya'];


try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $response = array();
    $query = $conn->prepare("SELECT nom, password FROM usuaris_app");
    /*$query->bindParam("correu", $nouEmail, PDO::PARAM_STR);
    $query->bindParam("credencial", $novaContra, PDO::PARAM_STR);*/
    $resultExecute = $query->execute();
    $row = $query->fetchAll(PDO::FETCH_ASSOC);
    $usuariExisteix = false;
    if ($row) {
        foreach ($row as $usuari) {
            if ($usuari['nom'] === $nouUsuari) {
                $usuariExisteix = true;
                if ($usuari['password'] === $novaContra) {
                    $response['estat'] = "OK";
                    $response['error'] = "";
                    $response['usuari_app'] = $nouUsuari;
                } else {
                    $response['estat'] = "KO";
                    $response['error'] = "Credencial incorrecte";
                    $response['usuari_app'] = $nouUsuari;
                }
                break;
            }
        }
        if (!$usuariExisteix) {
            $response['estat'] = "KO";
            $response['error'] = "Usuari no existeix";
            $response['usuari_app'] = $nouUsuari;
        }

        echo json_encode($response);
    }
} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
