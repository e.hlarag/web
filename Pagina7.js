$("#cançoButton").click(function(){
    event.preventDefault();
    $.ajax({
        url:"Pagina7Ordenar.php",
        method:"POST",
        data:{
            "canço":$("#cançoEscollida").val(),
        },
        dataType:"json",
        success: function (resultats){
            console.log(resultats);
            $("#result").empty();
            $.each(resultats, function(key, value) { //index representa la posicio i valor el valor
                actualitzarTaula(resultats);
            });
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);
        }

    })
})
function actualitzarTaula(resultats) {
    let tablaBody = $('#tableBody');
    tablaBody.empty();
    let TH = $('<tr>').css("border", "1px solid white"); //creació de la fila on van els th
    TH.append($('<th>').text("ID").css("border", "1px solid white"));//els inserim a la fila
    TH.append($('<th>').text("Nom cançó").css("border", "1px solid white"));
    TH.append($('<th>').text("Recaptació").css("border", "1px solid white"));
    tablaBody.append(TH);
    $.each(resultats, function (index, canco) {
        let fila = $('<tr>');//creació de la fila dels th
        fila.append($('<td>').html(canco.idSongs).css("border", "1px solid white"));//els inserim a la fila
        fila.append($('<td>').html(canco.Nom).css("border", "1px solid white"));
        fila.append($('<td>').html(canco.Recaptacio).css("border", "1px solid white"));
        tablaBody.append(fila);
    });
}