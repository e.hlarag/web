<?php
require_once("config.php");
$song=$_POST['canço'];
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $query = $conn->query("SELECT idSongs, Nom, Recaptacio FROM songs");
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $found = false;
    if ($song===""){
        $query = $conn->query("SELECT idSongs, Nom, Recaptacio  FROM songs order by Recaptacio desc, Nom asc, Reproduccions");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $resultExecute = $query->execute();
        echo json_encode($result);
    }else {
    foreach ($result as $cancion) {
        if ($cancion['Nom'] === $song) {
            $found = true;
        }
    }
    }
    if ($found) {
        $query = $conn->prepare("SELECT idSongs, Nom, Recaptacio FROM songs WHERE Nom =:song ORDER BY Nom ASC");
        $query->bindParam('song', $song, PDO::PARAM_STR);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($result);
    }

} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
