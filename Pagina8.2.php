<?php

require_once("config.php");

$resultats=array();



try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    //Usuari aleatori
    $queryUsuari = $conn->prepare("SELECT Id FROM usuaris_app ORDER BY RAND() LIMIT 1");
    $queryUsuari->execute();
    $UsuariRandom = $queryUsuari->fetch(PDO::FETCH_ASSOC);
    $usuari = $UsuariRandom['Id'];
   // print_r("L'usuari escollit és " . $usuari);

    //Cançó aleatoria
    $queryCanco = $conn->prepare("SELECT idSongs, Nom FROM songs ORDER BY RAND() LIMIT 1");
    $queryCanco->execute();
    $resultatCanco = $queryCanco->fetch(PDO::FETCH_ASSOC);
    $cancoRandom = $resultatCanco['idSongs'];
    $nombreCanco = $resultatCanco['Nom'];
    //print_r("La cançó escollida és " . $cancoRandom);

    //Existeix l'usuari?
    $query = $conn->prepare("SELECT * FROM usuaris_songs WHERE idusuari = :usuari");
    $query->bindParam(':usuari', $usuari, PDO::PARAM_INT);
    $query->execute();
    $resultat = $query->fetch(PDO::FETCH_ASSOC);

    //print_r("El resultat de la consulta és: ".  $query->rowCount());

    $cancoTrobada=false;
    $valorrepr = 1;

    if ($query->rowCount()>0) {//existeix
        //print_r($resultat['reproduccions'] . " " . $resultat['idusuari']);
        $reproduccionsArray = json_decode($resultat['reproduccions'], true);
        //print_r($reproduccionsArray);
        $cancoTrobada = false;
        for($i =0; $i<sizeof($reproduccionsArray); $i++) {
            if ($reproduccionsArray[$i]['song'] == $cancoRandom) {
                $reproduccionsArray[$i]['reproduccions'] += 1;
                $cancoTrobada=true;
                //echo "La posició trobada és ". $i;
            }
        }

        if (!$cancoTrobada) {
            $reproduccionsArray[] = array('song' => $cancoRandom, 'reproduccions' => $valorrepr);
            $JsonReproduccionsArray = json_encode($reproduccionsArray);

            //print_r($cancoRandom);
           // print_r($JsonReproduccionsArray);

            $queryUpdate = $conn->prepare("UPDATE usuaris_songs SET reproduccions=:reproduccionsJSON WHERE idusuari=:usuari");
            $queryUpdate->bindParam('reproduccionsJSON', $JsonReproduccionsArray, PDO::PARAM_STR);
            $queryUpdate->bindParam('usuari', $usuari, PDO::PARAM_INT);

            $resultUpdateReproductions = $queryUpdate->execute();


            $query=$conn->prepare("Update songs set Reproduccions=Reproduccions+1, Recaptacio=Recaptacio+2 where idSongs=:canco");
            $query->bindParam('canco', $cancoRandom, PDO::PARAM_INT);
            $query->execute();



            $missatge=array("missatge"=> "L'usuari ".$usuari. " ha incrementat les reproduccions de aquesta canco: ".$nombreCanco);
            array_push($resultats,$missatge);
            print_r(json_encode($resultats));
            //echo "entro -1";

        }else{
            for($i =0; $i<sizeof($reproduccionsArray); $i++) {
                if ($reproduccionsArray[$i]['song'] == $cancoRandom) {
                    $reproduccionsArray[$i]['reproduccions'] += 1;
                    //echo "La posició trobada és ". $i;
                }
            }
            $JsonReproduccionsArray=json_encode($reproduccionsArray);
            //echo "Entro 0";
            //print_r($JsonReproduccionsArray);
            $queryUpdate = $conn->prepare("UPDATE usuaris_songs SET reproduccions=:reproduccionsJSON WHERE idusuari=:usuari");
            $queryUpdate->bindParam('reproduccionsJSON', $JsonReproduccionsArray, PDO::PARAM_STR);
            $queryUpdate->bindParam('usuari', $usuari, PDO::PARAM_INT);

            $resultUpdateReproductions = $queryUpdate->execute();

            $query=$conn->prepare("Update songs set Reproduccions=Reproduccions+1, Recaptacio=Recaptacio+2 where idSongs=:canco");
            $query->bindParam('canco', $cancoRandom, PDO::PARAM_INT);
            $query->execute();
            $missatge=array("missatge"=> "S'han actualitzat les reproduccions de l'usuari ".$usuari);
            array_push($resultats,$missatge);
            print_r(json_encode($resultats));

        }
    } else if (!$resultat){//no existeix
        $reproduccionsArray[] = array('song' => $cancoRandom, 'reproduccions' => $valorrepr);
        $JsonReproduccionsArray = json_encode($reproduccionsArray);
       // echo"Entro1";

        $queryInsert = $conn->prepare("INSERT INTO usuaris_songs (idusuari) VALUES (:usuari)");
        $queryInsert->bindParam('usuari', $usuari, PDO::PARAM_INT);
        $resultInsertReproductions = $queryInsert->execute();

        $queryUpdateSongs = $conn->prepare("UPDATE songs SET Reproduccions = Reproduccions + 1, Recaptacio = Recaptacio + 2 WHERE idSongs = :canco");
        $queryUpdateSongs->bindParam('canco', $cancoRandom, PDO::PARAM_INT);
        $queryUpdateSongs->execute();
        $missatge=array("missatge"=>"S'ha afegit un nou registre la BD per les reproduccions de l'usuari ".$usuari. " i la canco ".$nombreCanco);
        array_push($resultats,$missatge);
        print_r(json_encode($resultats));

    }else{
        echo "Aqui  no pot entrar i no s'ha de fer res";
        /*
        echo "entro 2";
        $queryUpdate2 = $conn->prepare("UPDATE usuaris_songs SET reproduccions=:reproduccionsJSON WHERE idusuari=:usuari");
        $queryUpdate2->bindParam('usuari', $usuari, PDO::PARAM_INT);
        $queryUpdate2->bindParam('reproduccionsJSON', $JsonReproduccionsArray, PDO::PARAM_STR);
        $resultUpdateReproductions = $queryUpdate2->execute();

        $query=$conn->prepare("Update songs set Reproduccions=Reproduccions+1, Recaptacio=Recaptacio+2 where idSongs=:canco");
        $query->bindParam('canco', $cancoRandom, PDO::PARAM_INT);
        $query->execute();

        $ultimMissatge=array("missatge"=>"L'usuari".$usuari." ha incrementat les reproduccions d'aquesta canco: ".$nombreCanco);
        array_push($resultats,$ultimMissatge);
        print_r(json_encode($resultats));
        */

    }
}catch(PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}
