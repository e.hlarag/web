setInterval(hora, 1000);
hora();

function hora() {
    const ahora = new Date();
    const hora = ahora.getHours().toString();

    let minutos = ahora.getMinutes().toString();
    if (minutos.length < 2) {
        minutos = '0' + minutos;
    }

    let segundos = ahora.getSeconds().toString();
    if (segundos.length < 2) {
        segundos = '0' + segundos;
    }


    const fecha = `${ahora.getDate()}/${ahora.getMonth() + 1}/${ahora.getFullYear()}`;

    const html = `${hora}:${minutos}:${segundos} - ${fecha}`;


    const relojElement = document.getElementById("reloj");

    relojElement.innerHTML = html;

    relojElement.style.fontSize = "25px";
}
