$(document).ready(function() {
    var loggedInUser = localStorage.getItem('loggedInUser');
    if (loggedInUser !== null) {
        console.log("Usuari autenticat: " + loggedInUser);
    }
});

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

$("#registerButton").click(function() {
    event.preventDefault();
        $.ajax({
            method: "POST",
            url: "REGISTER.php",
            data: {
                "nombre": $("#registerUsername").val(),
                "contrassenya": $("#registerPassword").val(),
            },
            dataType: "json",
            success: async function (response) {
                console.log(response);
                if (response.estat === "OK") {
                    $("#result").html("Registre exitós per a l'usuari: " + response.usuari_app).css("color", "green");
                    await sleep(1000);
                    $("#result").empty();
                    $("#result").html("Redirigint...").css("color", "green");
                    localStorage.setItem('loggedInUser', response.usuari_app);
                    console.log(localStorage);
                    await sleep(3000);
                    window.location.href = "index.html";
                } else {
                    $("#result").html("Error: " + response.error + " per a l'usuari: " + response.usuari_app).css("color", "red");
                }
            },
            error: function (jqXHR, textStatus, error) {
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);
            }

        })
    });

$("#cancelButton").click(function (){
window.location.href="index.html";
});

$("#loginButton").click(function() {
    event.preventDefault();
    if (localStorage.getItem('loggedInUser') !== null) {
        $("#resultat").html("Ja tens una sessió oberta").css("color", "red");
    }else{
        $.ajax({
            method: "POST",
            url: "Login.php",
            data: {
                "nombre": $("#username").val(),
                "contrassenya": $("#password").val(),
            },
            dataType: "json",
            success: async function (response) {
                console.log(response);
                if (response.estat === "OK") {
                    $("#resultat").html("Login exitós per a l'usuari: " + response.usuari_app).css("color", "green");
                    await sleep(1000);
                    localStorage.setItem('loggedInUser', response.usuari_app);
                    console.log(localStorage);
                    $("#resultat").html("Redirigint...");
                    await sleep(3000);
                    window.location.href = "index.html";
                    //let loggedInUser = localStorage.getItem('loggedInUser');
                    //window.location.href="Pagina1.html";
                } else {
                    $("#resultat").html("Error: " + response.error + " per a l'usuari: " + response.usuari_app).css("color", "red");
                }
            },
            error: function (jqXHR, textStatus, error) {
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);
            }

        });
    }
});

document.addEventListener('DOMContentLoaded', function() {
    var loggedInUser = localStorage.getItem('loggedInUser');
    if (loggedInUser) {
        $("#dropMenuB1").html("Usuari: " + loggedInUser);
        $("#dropMenuB1").next(".dropdown-menu").html(`
            <li><a class="dropdown-item" id="logoutbtn" href="Pagina6.1Register.html">Log Out</a></li>
        `);
        $("#logoutbtn").click(function () {
            localStorage.clear();
        });
    } else {
        $("#dropMenuB1").html("Iniciar Sessió");
        $("#dropMenuB1").next(".dropdown-menu").html(`
            <li><a class="dropdown-item" href="Pagina6.1Register.html">Registrar</a></li>
            <li><a class="dropdown-item" href="Pagina6LoginR.html">Iniciar Sessió</a></li>
        `);
    }
});


