<?php
require_once("config.php");
$song=$_POST['canço'];
try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $query = $conn->prepare("SELECT * FROM songs WHERE Nom =:song");
    $query->bindParam('song', $song, PDO::PARAM_STR);
    $resultExecute = $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    if ($result) {
        foreach ($result as $row) {
            echo "La cançó ja existeix a la BD: " . $row['Nom'];
        }
    } else if ($song !== "") {
        $query = $conn->prepare("INSERT INTO songs (Nom) VALUES (:song)");
        $query->bindParam(':song', $song, PDO::PARAM_STR);
        $resultExecute = $query->execute();

        if ($resultExecute) {
            echo "CANÇO INSERIDA CORRECTAMENT";
        } else {
            echo "Error a l'inserir la cançó.";
        }
    } else {
        echo "Siusplau introdueix una cancó.";
    }
} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
