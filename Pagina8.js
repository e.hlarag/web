$("#afegirCancoButton").click(function(){
    event.preventDefault();
    $.ajax({
        url:"Pagina8.php",
        method:"POST",
        data:{
            "canço":$("#novaCanco").val(),
        },
        dataType:"text",
        success: function (resultats){
            console.log(resultats);
            $("#resultat").html(resultats)
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);
        }

    })
})
$("#validarCancoButton").click(function() {
    $.ajax({
        url: 'Pagina8.2.php',
        dataType: 'json',
        success: function(response) {
            console.log(response);
            $('#resultat').empty();
            //accedir als missatges dins de l'array
            for (let i = 0; i < response.length; i++) {
                $('#resultat').append(response[i].missatge + "<br>");
            }
        },

        error: function(jqXHR, textStatus, errorThrown) {
            console.log('Error: ' + textStatus + ' ' + errorThrown);
        }
    });
});